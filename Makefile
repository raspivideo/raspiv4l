OBJS=raspiv4l.o
BIN=raspiv4l.bin

CFLAGS+=-DSTANDALONE -D__STDC_CONSTANT_MACROS -D__STDC_LIMIT_MACROS -DTARGET_POSIX -D_LINUX -fPIC -DPIC 
CFLAGS+=-D_REENTRANT -D_LARGEFILE64_SOURCE -D_FILE_OFFSET_BITS=64 -U_FORTIFY_SOURCE -Wall -DHAVE_LIBOPENMAX=2
CFLAGS+=-DOMX -DOMX_SKIP64BIT -ftree-vectorize -pipe -DUSE_EXTERNAL_OMX -DHAVE_LIBBCM_HOST -DUSE_EXTERNAL_LIBBCM_HOST 
CFLAGS+=-DUSE_VCHIQ_ARM -Wno-psabi -g 

LDFLAGS+=-L/opt/vc/src/hello_pi/libs/ilclient -lswscale -lavcodec -lilclient -L$(SDKSTAGE)/opt/vc/lib/ -lopenmaxil
LDFLAGS+=-lbcm_host -lvcos -lvchiq_arm -lpthread -lrt -L../libs/ilclient -L../libs/vgfont -lavutil

INCLUDES+=-I/opt/vc/src/hello_pi/libs/ilclient/ -I$(SDKSTAGE)/opt/vc/include/ 
INCLUDES+=-I$(SDKSTAGE)/opt/vc/include/interface/vcos/pthreads 
INCLUDES+=-I$(SDKSTAGE)/opt/vc/include/interface/vmcs_host/linux

all: $(BIN) $(LIB)

%.o: %.c
	@rm -f $@ 
	$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@ -Wno-deprecated-declarations

%.o: %.cpp
	@rm -f $@ 
	$(CXX) $(CFLAGS) $(INCLUDES) -c $< -o $@ -Wno-deprecated-declarations

%.bin: $(OBJS)
	$(CC) -o $@ -Wl,--whole-archive $(OBJS) $(LDFLAGS) -Wl,--no-whole-archive -rdynamic

%.a: $(OBJS)
	$(AR) r $@ $^

clean:
	for i in $(OBJS); do (if test -e "$$i"; then ( rm $$i ); fi ); done
	@rm -f $(BIN) $(LIB)


